# Inicio

## Requerimientos

* Node.js >= 6.10
* nmp > 3.0
* Java 8
* Android Studio 3.1.2

## Instalación

### Instalación de ionic

```
npm install -g ionic
```

### Usted necesita clonar el repositorio:

```
$ git clone git@github.com:phalcon/invo.git
```

### Instalar dependencias (entrar a la raíz del proyecto)

```
npm install
```

### Iniciar el proyecto

```
ionic serve --lab
```

## Imágenes de la aplicación

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/menu-1.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/menu-2.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/calculadora.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/matrices.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/ecuacion-tercer-grado.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/transformar-unidades.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/graficas.png)

![new director](https://bitbucket.org/luismsg2110/app-calculator/downloads/doc.png)