import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { CalculatorPage } from '../pages/calculator/calculator';
import { ComplexPage } from '../pages/complex/complex';
import { MatrixPage } from '../pages/matrix/matrix';
import { PitagorasPage } from '../pages/pitagoras/pitagoras';
import { EquationtwoPage } from '../pages/equationtwo/equationtwo';
import { EquationthreePage } from '../pages/equationthree/equationthree';
import { TspacePage } from '../pages/tspace/tspace';
import { TtimePage } from '../pages/ttime/ttime';
import { TmasaPage } from '../pages/tmasa/tmasa';
import { TtemperaturePage } from '../pages/ttemperature/ttemperature';
import { SincosPage } from '../pages/sincos/sincos';
import { PlotfunPage } from '../pages/plotfun/plotfun';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    CalculatorPage,
    ComplexPage,
    MatrixPage,
    PitagorasPage,
    EquationtwoPage,
    EquationthreePage,
    TspacePage,
    TtimePage,
    TmasaPage,
    TtemperaturePage,
    SincosPage,
    PlotfunPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
