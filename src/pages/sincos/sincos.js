var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
/**
 * Generated class for the SincosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SincosPage = /** @class */ (function () {
    function SincosPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.variables = [];
        this.permit = false;
        this.showres = false;
        this.varOne = '';
        this.varTwo = '';
        this.varTree = '';
    }
    SincosPage.prototype.onChange = function () {
        if (this.variables.length != 3) {
            this.showAlert();
            this.permit = false;
        }
        else {
            if (this.variables[0] === 'a') {
                this.varOne = 'a';
            }
            else if (this.variables[0] === 'b') {
                this.varOne = 'b';
            }
            else if (this.variables[0] === 'c') {
                this.varOne = 'c';
            }
            else if (this.variables[0] === 'alfa') {
                this.varOne = 'α';
            }
            else if (this.variables[0] === 'beta') {
                this.varOne = 'β';
            }
            else if (this.variables[0] === 'gamma') {
                this.varOne = 'α';
            }
        }
    };
    SincosPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Atención!',
            subTitle: 'Debe seleccionar tres variables para realizar el cálculo',
            buttons: ['OK']
        });
        alert.present();
    };
    SincosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-sincos',
            templateUrl: 'sincos.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AlertController])
    ], SincosPage);
    return SincosPage;
}());
export { SincosPage };
//# sourceMappingURL=sincos.js.map