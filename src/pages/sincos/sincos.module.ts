import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SincosPage } from './sincos';

@NgModule({
  declarations: [
    SincosPage,
  ],
  imports: [
    IonicPageModule.forChild(SincosPage),
  ],
})
export class SincosPageModule {}
