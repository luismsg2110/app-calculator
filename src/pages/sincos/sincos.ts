import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the SincosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sincos',
  templateUrl: 'sincos.html',
})
export class SincosPage {
  variables:any = [];
  permit:boolean = false;
  showres:boolean = false;

  varOne:any = '';
  varTwo:any = '';
  varTree:any = '';

  value1:number = 0;
  value2:number = 0;
  value3:number = 0;

  valuea:number = 0;
  valueb:number = 0;
  valuec:number = 0;
  valuealfa:number = 0;
  valuebeta:number = 0;
  valuegamma:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  onChange(){
  	this.showres = false;
  	if(this.variables.length != 3){
        this.showAlert();
        this.permit = false;
     } else {
     	this.permit = true;

     	if (this.variables[0] === 'a'){
      		this.varOne = 'a';
      	} else if (this.variables[0] === 'b'){
      		this.varOne = 'b';
      	} else if (this.variables[0] === 'c') {
      		this.varOne = 'c';
      	} else if (this.variables[0] === 'alfa'){
      		this.varOne = 'α';
      	} else if (this.variables[0] === 'beta'){
      		this.varOne = 'β';
      	} else if (this.variables[0] === 'gamma'){
      		this.varOne = 'γ';
      	}

      	if (this.variables[1] === 'a'){
      		this.varTwo = 'a';
      	} else if (this.variables[1] === 'b'){
      		this.varTwo = 'b';
      	} else if (this.variables[1] === 'c') {
      		this.varTwo = 'c';
      	} else if (this.variables[1] === 'alfa'){
      		this.varTwo = 'α';
      	} else if (this.variables[1] === 'beta'){
      		this.varTwo = 'β';
      	} else if (this.variables[1] === 'gamma'){
      		this.varTwo = 'γ';
      	}

      	if (this.variables[2] === 'a'){
      		this.varTree = 'a';
      	} else if (this.variables[2] === 'b'){
      		this.varTree = 'b';
      	} else if (this.variables[2] === 'c') {
      		this.varTree = 'c';
      	} else if (this.variables[2] === 'alfa'){
      		this.varTree = 'α';
      	} else if (this.variables[2] === 'beta'){
      		this.varTree = 'β';
      	} else if (this.variables[2] === 'gamma'){
      		this.varTree = 'γ';
      	}

     }
     if ( this.variables[0] ===  'alfa' && this.variables[1] === 'beta' && this.variables[2] === 'gamma' ) {
     	this.permit = false;
     	this.showres = false;
     	this.showAlert2();
     }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Atención!',
      subTitle: 'Debe seleccionar tres variables para realizar el cálculo',
      buttons: ['OK']
    });
    alert.present();
  }

  showAlert2() {
    const alert = this.alertCtrl.create({
      title: 'Atención!',
      subTitle: 'Debe seleccionar almenos un lado',
      buttons: ['OK']
    });
    alert.present();
  }

  calSinCos() {
  	this.showres = true;
  	if (this.varOne === 'a') {
  		if (this.varTwo === 'b') {
  			if (this.varTree === 'c') {
  				var a = this.valuea = this.value1;
  				var b = this.valueb = this.value2;
  				var c = this.valuec = this.value3;
  				this.valuealfa = this.calAngulo(a, b, c);
  				this.valuebeta = this.calAngulo(b, a, c);
  				this.valuegamma = this.calAngulo(c, a, b);
  			} else if ( this.varTree === 'α') {
  				this.valuea = this.value1;
  				this.valueb = this.value2;
  				this.valuealfa = this.value3;
  				this.valuebeta = this.calLeySin(this.valueb, this.valuea, this.valuealfa);
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  				this.valueb = this.calLeyCos(this.valuea, this.valueb, this.valuebeta);
  			} else if ( this.varTree === 'β') {
  				this.valuea = this.value1;
  				this.valueb = this.value2;
  				this.valuebeta = this.value3;
  				this.valuealfa = this.calLeySin(this.valuea, this.valueb, this.valuebeta);
 				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  				this.valuec = this.calLeyCos(this.valuea, this.valueb, this.valuegamma);
  			} else if ( this.varTree === 'γ') {
  				this.valuea = this.value1;
  				this.valueb = this.value2;
  				this.valuegamma = this.value3;
  				this.valuec = this.calLeyCos (this.valuea, this.valueb, this.valuegamma);
  				this.valuealfa = this.calLeySin(this.valuea, this.valuec, this.valuegamma);	
  				this.valuebeta = 180 - this.valuegamma - this.valuealfa;
  			}
  		} else if ( this.varTwo === 'c') {
  			if ( this.varTree === 'α' ) {
  				this.valuea = this.value1;
  				this.valuec = this.value2;
  				this.valuealfa = this.value3;
  				this.valuegamma = this.calLeySin(this.valuec, this.valuea, this.valuealfa);
  				this.valuebeta = 180 - this.valuealfa - this.valuegamma;
  				this.valueb = this.calLeyCos(this.valuea, this.valuec, this.valuebeta);
  			} else if ( this.varTree === 'β') {
  				this.valuea = this.value1;
  				this.valuec = this.value2;
  				this.valuebeta = this.value3;
  				this.valueb = this.calLeyCos(this.valuea, this.valuec, this.valuebeta);
  				this.valuealfa = this.calLeySin(this.valuea, this.valueb, this.valuebeta);
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  			} else if ( this.varTree === 'γ') {
  				this.valuea = this.value1;
  				this.valuec = this.value2;
  				this.valuegamma = this.value3;
  				this.valuealfa = this.calLeySin(this.valuea, this.valuec, this.valuegamma);
  				this.valuebeta = 180 - this.valuealfa - this.valuegamma;
  				this.valueb = this.calLeyCos(this.valuea, this.valuec, this.valuebeta);
  			}
  		} else if ( this.varTwo === 'α') {
  			if ( this.varTree === 'β') {
  				this.valuea = this.value1;
  				this.valuealfa = this.value2;
  				this.valuebeta = this.value3;
  				this.valueb = this.calLeySin2(this.valuebeta, this.valuea, this.valuealfa);
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  				this.valuec = this.calLeyCos(this.valuea, this.valueb, this.valuegamma);
  			} else if ( this.varTree === 'γ') {
  				this.valuea = this.value1;
  				this.valuealfa = this.value2;
  				this.valuegamma = this.value3;
  				this.valuec = this.calLeySin2(this.valuegamma, this.valuea, this.valuealfa);
  				this.valuebeta = 180 - this.valuealfa - this.valuegamma;
  				this.valueb = this.calLeyCos(this.valuea, this.valuec, this.valuebeta);
  			}
  		} else if ( this.varTwo === 'β') {
  			if ( this.varTree === 'γ' ) {
  				this.valuea = this.value1;
  				this.valuebeta = this.value2;
  				this.valuegamma  = this.valuegamma;
  				this.valuealfa = 180 - this.valuebeta - this.valuegamma;
  				this.valueb = this.calLeySin2(this.valuebeta, this.valuea, this.valuealfa);
  				this.valuec = this.calLeySin2(this.valuegamma, this.valuea, this.valuealfa);
  			}
  		}
  	} else if ( this.varOne === 'b') {
  		if ( this.varTwo === 'c') {
  			if ( this.varTree === 'α') {
  				this.valueb = this.value1;
  				this.valuec = this.value2;
  				this.valuealfa = this.value3;
  				this.valuea = this.calLeyCos(this.valueb, this.valuec, this.valuealfa);
  				this.valuebeta = this.calLeySin(this.valueb, this.valuea, this.valuealfa);
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  			} else if ( this.varTree === 'β') {
  				this.valueb = this.value1;
  				this.valuec = this.value2;
  				this.valuebeta = this.value3;
  				this.valuegamma = this.calLeySin(this.valuec, this.valueb, this.valuebeta);
  				this.valuealfa = 180 - this.valuebeta - this.valuegamma;
  				this.valuea = this.calLeyCos(this.valueb, this.valuec, this.valuealfa);
  			} else if ( this.varTree === 'γ') {
  				this.valueb = this.value1;
  				this.valuec = this.value2;
  				this.valuegamma = this.value3;
  				this.valuebeta = this.calLeySin(this.valueb, this.valuec, this.valuegamma);
  				this.valuealfa = 180 - this.valuebeta - this.valuegamma;
  				this.valuea = this.calLeyCos(this.valueb, this.valuec, this.valuealfa);
  			}
  		} else if ( this.varTwo === 'α') {
  			if ( this.varTree === 'β') {
  				this.valueb = this.value1;
  				this.valuealfa = this.value2;
  				this.valuebeta = this.value3;
  				this.valuea = this.calLeySin2(this.valuealfa, this.valueb, this.valuebeta);
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  				this.valuec = this.calLeyCos(this.valuea, this.valueb, this.valuegamma);
  			} else if ( this.varTree === 'γ' ) {
  				this.valueb = this.value1;
  				this.valuealfa = this.value2;
  				this.valuegamma = this.value3;
  				this.valuebeta = 180 - this.valuealfa - this.valuegamma;
  				this.valuea = this.calLeySin2(this.valuealfa, this.valueb, this.valuebeta);
  				this.valuec = this.calLeySin2(this.valuegamma, this.valueb, this.valuebeta);
  			}
  		} else if ( this.varTwo === 'β') {
  			if ( this.varTree === 'γ') {
  				this.valueb = this.value1;
  				this.valuebeta = this.value2;
  				this.valuegamma = this.value3;
  				this.valuec = this.calLeySin2(this.valuegamma, this.valueb, this.valuebeta);
  				this.valuealfa = 180 - this.valuebeta - this.valuegamma;
  				this.valuea = this.calLeySin2(this.valuealfa, this.valueb, this.valuebeta);
  			}
  		}
  	} else if ( this.varOne === 'c') {
  		if ( this.varTwo === 'α') {
  			if ( this.varTree === 'β') {
  				this.valuec = this.value1;
  				this.valuealfa = this.value2;
  				this.valuebeta = this.value3;
  				this.valuegamma = 180 - this.valuealfa - this.valuebeta;
  				this.valueb = this.calLeySin2(this.valuebeta, this.valuec, this.valuegamma);
  				this.valuea = this.calLeySin2(this.valuealfa, this.valuec, this.valuegamma);
  			} else if ( this.varTree === 'γ') {
  				this.valuec = this.value1;
  				this.valuealfa = this.value2;
  				this.valuegamma = this.value3;
  				this.valuea = this.calLeySin2(this.valuealfa, this.valuec, this.valuegamma);
  				this.valuebeta = 180 - this.valuealfa - this.valuegamma;
  				this.valueb = this.calLeySin2(this.valuebeta, this.valuec, this.valuegamma);
  			}
  		} else if ( this.varTwo === 'β') {
  			if ( this.varTree === 'γ') {
  				this.valuec = this.value1;
  				this.valuebeta = this.value2;
  				this.valuegamma = this.value3;
  				this.valuealfa = 180 - this.valuebeta - this.valuegamma;
  				this.valueb = this.calLeySin2(this.valuebeta, this.valuec, this.valuegamma);
  				this.valuea = this.calLeySin2(this.valuealfa, this.valuec, this.valuegamma);	
  			}
  		}
  	} 

  }

// x es el lado opuesto del angulo a calcluar
// Cuando tenemos todos los lados
  calAngulo( x, y, z) {
  	var y2 = y*y;
  	var x2 = x*x;
  	var z2 = z*z;
  	var den = 2*y*z;
  	var cal = (y2 - x2 + z2)/den;
  	var angu = Math.acos(cal);
  	angu = this.degress(angu);
  	if ( angu < 0) {
  		angu += 180;
  	}
  	return angu;
  }

// Cuando tenemos un ángulo y dos lados
// x es el lado opuesto del angulo a calucular
// y es el otro lado y z su angulo
  calLeySin( x, y, z) {
  	var angu = Math.asin((x*Math.sin(this.radians(z)))/y);
  	angu = this.degress(angu);
  	return angu;
  }

// Cuando tenemos dos ángulos y un lado de ese angulo
// x es el angulo del lado a calcular
// "y" es el lado y "z" su angulo conocido
  calLeySin2( x, y, z) {
  	var lado = (y*Math.sin(this.radians(x)))/Math.sin(this.radians(z));
  	return lado;
  }

//Cuando tenemos un lado con su angulo y otro lado 
// "x" y "y" son los lados conocidos
// z es el angulo opuesto del lado a colcular
 calLeyCos( x, y, z) {
 	var lado = Math.sqrt((x*x) + (y*y) - (2*x*y*Math.cos(this.radians(z))));
 	return lado;
 }

  degress(radians) {
  	return radians * 180 / Math.PI;
  }

  radians(degrees) {
	return degrees * Math.PI / 180;
  }

}
