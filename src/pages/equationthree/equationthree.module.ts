import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EquationthreePage } from './equationthree';

@NgModule({
  declarations: [
    EquationthreePage,
  ],
  imports: [
    IonicPageModule.forChild(EquationthreePage),
  ],
})
export class EquationthreePageModule {}
