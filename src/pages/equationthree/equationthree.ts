import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EquationthreePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-equationthree',
  templateUrl: 'equationthree.html',
})
export class EquationthreePage {
  valuea:number = 0;
  valueb:number = 0;
  valuec:number = 0;
  valued:number = 0;
  real1:number = 0;
  real2:number = 0;
  real3:number = 0;
  img1:number = 0;
  img2:number = 0;
  img3:number = 0;
  sign1:string = '';
  sign2:string = '';
  sign3:string = '';
  showi1:boolean = false;
  showi2:boolean = false;
  showi3:boolean = false;
  showresult:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calEquationThree(){
    this.showresult = true;
  	var result = this.cubicSolve(this.valuea, this.valueb, this.valuec, this.valued);
    this.real1 = result[0].real;
    this.real2 = result[1].real;
    this.real3 = result[2].real;
    this.img1 = Math.abs(result[0].i);
    this.img2 = Math.abs(result[1].i);
    this.img3 = Math.abs(result[2].i);
    this.sign1 = this.getSign(result[0].i);
    this.sign2 = this.getSign(result[1].i);
    this.sign3 = this.getSign(result[2].i);0
    if (this.img1 === 0){
      this.showi1 = false;
    } else {
      this.showi1 = true;
    }
    if (this.img2 === 0){
      this.showi2 = false;
    } else {
      this.showi2 = true;
    }
    if (this.img3 === 0){
      this.showi3 = false;
    } else {
      this.showi3 = true;
    }
  }

cubicSolve(a, b, c, d){

  b /= a;
  c /= a;
  d /= a;

  var discrim, q, r, dum1, s, t, term1, r13;

  q = (3.0*c - (b*b))/9.0;
  r = -(27.0*d) + b*(9.0*c - 2.0*(b*b));
  r /= 54.0;

  discrim = q*q*q + r*r;
  
  var roots = [ {real: 0, i: 0}, {real: 0, i: 0}, {real: 0, i: 0} ]
  
  term1 = (b/3.0);

  if (discrim > 0) { // one root real, two are complex
   s = r + Math.sqrt(discrim);
   s = ((s < 0) ? -Math.pow(-s, (1.0/3.0)) : Math.pow(s, (1.0/3.0)));
   t = r - Math.sqrt(discrim);
   t = ((t < 0) ? -Math.pow(-t, (1.0/3.0)) : Math.pow(t, (1.0/3.0)));
   
   roots[0].real = -term1 + s + t;
   term1 += (s + t)/2.0;
   roots[2].real = roots[1].real = -term1;
   term1 = Math.sqrt(3.0)*(-t + s)/2;
   
   roots[1].i = term1;
   roots[2].i = -term1;
   console.log("Caso discriminante > 0");
   return roots;
  } // End if (discrim > 0)

  // The remaining options are all real
  

  if (discrim == 0){ // All roots real, at least two are equal.
   r13 = ((r < 0) ? -Math.pow(-r,(1.0/3.0)) : Math.pow(r,(1.0/3.0)));
   roots[0].real = -term1 + 2.0*r13;
   roots[2].real = roots[1].real = -(r13 + term1);
   console.log("Caso discriminante = 0");
   return roots;
  } // End if (discrim == 0)

  // Only option left is that all roots are real and unequal (to get here, q < 0)
  q = -q;
  dum1 = q*q*q;
  dum1 = Math.acos(r/Math.sqrt(dum1));
  r13 = 2.0*Math.sqrt(q);
  
  roots[0].real = -term1 + r13*Math.cos(dum1/3.0);
  roots[1].real = -term1 + r13*Math.cos((dum1 + 2.0*Math.PI)/3.0);
  roots[2].real = -term1 + r13*Math.cos((dum1 + 4.0*Math.PI)/3.0);
  console.log("Caso discriminante < 0");
  return roots;
  }  

  private getSign(val){
    if (val < 0) {
      return '-';
    } else {
      return '+';
    }
  }
}