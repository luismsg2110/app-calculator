import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PitagorasPage } from './pitagoras';

@NgModule({
  declarations: [
    PitagorasPage,
  ],
  imports: [
    IonicPageModule.forChild(PitagorasPage),
  ],
})
export class PitagorasPageModule {}
