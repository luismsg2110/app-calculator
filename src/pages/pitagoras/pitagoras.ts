import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the PitagorasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pitagoras',
  templateUrl: 'pitagoras.html',
})
export class PitagorasPage {
  varOne:any = '';
  varTwo:any = '';
  variables:any=[];
  permit:boolean = false;
  showres:boolean = false;
  valueOne:any = 0;
  valueTwo:any = 0;
  valueCA:any = 0;
  valueCO:any = 0;
  valueH:any = 0;
  valueTH:any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  onChange(){
  	this.showres = false;
  	if(this.variables.length != 2){
        this.showAlert();
        this.permit = false;
     } else {

      	if (this.variables[0] === 'co'){
      		this.varOne = 'CO';
      	} else if (this.variables[0] === 'ca'){
      		this.varOne = 'CA';
      	} else if (this.variables[0] === 'h') {
      		this.varOne = 'H';
      	} else if (this.variables[0] === 'theeta'){
      		this.varOne = 'θ';
      	}

      	if (this.variables[1] === 'co'){
      		this.varTwo = 'CO';
      	} else if (this.variables[1] === 'ca'){
      		this.varTwo = 'CA';
      	} else if (this.variables[1] === 'h') {
      		this.varTwo = 'H';
      	} else if (this.variables[1] === 'theeta'){
      		this.varTwo = 'θ';
      	}

      	this.permit = true;
     }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Atención!',
      subTitle: 'Debe seleccionar sólo dos variables para realizar el cálculo',
      buttons: ['OK']
    });
    alert.present();
  }

  calPitagoras(){
  	this.showres = true;
  	if (this.varOne === 'CA'){
  		if (this.varTwo === 'CO'){
  			this.valueCA = this.valueOne;
  			this.valueCO = this.valueTwo;
  			this.valueH = Math.sqrt((this.valueCA*this.valueCA) + (this.valueCO*this.valueCO)).toFixed(5);
  			this.valueTH = this.degress(Math.atan(this.valueCO / this.valueCA)).toFixed(5);
  		} else if (this.varTwo === 'H') {
  			this.valueCA = this.valueOne;
  			this.valueH = this.valueTwo;
  			this.valueCO = Math.sqrt((this.valueH*this.valueH) - (this.valueCA*this.valueCA)).toFixed(5);
  			this.valueTH = this.degress(Math.acos(this.valueCA / this.valueH)).toFixed(5);
  		} else if (this.varTwo === 'θ') {
  			this.valueCA = this.valueOne;
  			this.valueTH = this.valueTwo;
  			var valueTHR = this.radians(this.valueTwo);
  			this.valueCO = (Math.tan(valueTHR)*this.valueCA).toFixed(5);
  			this.valueH = Math.sqrt((this.valueCA*this.valueCA) + (this.valueCO*this.valueCO)).toFixed(5);
  		}
  	} else if (this.varOne === 'CO') {
  		if (this.varTwo === 'H') {
  			this.valueCO = this.valueOne;
  			this.valueH = this.valueTwo;
  			this.valueCA = Math.sqrt((this.valueH*this.valueH) - (this.valueCO*this.valueCO)).toFixed(5);
  			this.valueTH = this.degress(Math.atan(this.valueCO / this.valueCA)).toFixed(5);
  		} else if (this.varTwo === 'θ') {
  			this.valueCO = this.valueOne;
  			this.valueTH = this.valueTwo;
  			var valueTHR = this.radians(this.valueTwo);
  			this.valueCA = (this.valueCO/Math.tan(valueTHR)).toFixed(5);
  			this.valueH = Math.sqrt((this.valueCA*this.valueCA) + (this.valueCO*this.valueCO)).toFixed(5);
  		}
  	} else if (this.varOne === 'H') {
  		if (this.varTwo === 'θ') {
  			this.valueH = this.valueOne;
  			this.valueTH = this.valueTwo;
  			var valueTHR = this.radians(this.valueTwo);
  			this.valueCO = (this.valueH*Math.sin(valueTHR)).toFixed(5);
  			this.valueCA = (this.valueH*Math.cos(valueTHR)).toFixed(5); 
  		}
  	} 
  }

  degress(radians){
  	return radians * 180 / Math.PI;
  }

  radians(degrees) {
	return degrees * Math.PI / 180;
  };

}
