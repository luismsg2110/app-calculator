var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the PitagorasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PitagorasPage = /** @class */ (function () {
    function PitagorasPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.varOne = '';
        this.varTwo = '';
        this.variables = [];
        this.permit = false;
        this.showres = false;
        this.valueOne = 0;
        this.valueTwo = 0;
        this.valueCA = 0;
        this.valueCO = 0;
        this.valueH = 0;
        this.valueTH = 0;
    }
    PitagorasPage.prototype.onChange = function () {
        this.showres = false;
        if (this.variables.length != 2) {
            this.showAlert();
            this.permit = false;
        }
        else {
            if (this.variables[0] === 'co') {
                this.varOne = 'CO';
            }
            else if (this.variables[0] === 'ca') {
                this.varOne = 'CA';
            }
            else if (this.variables[0] === 'h') {
                this.varOne = 'H';
            }
            else if (this.variables[0] === 'theeta') {
                this.varOne = 'θ';
            }
            if (this.variables[1] === 'co') {
                this.varTwo = 'CO';
            }
            else if (this.variables[1] === 'ca') {
                this.varTwo = 'CA';
            }
            else if (this.variables[1] === 'h') {
                this.varTwo = 'H';
            }
            else if (this.variables[1] === 'theeta') {
                this.varTwo = 'θ';
            }
            this.permit = true;
        }
    };
    PitagorasPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Atención!',
            subTitle: 'Debe seleccionar sólo dos variables para realizar el cálculo',
            buttons: ['OK']
        });
        alert.present();
    };
    PitagorasPage.prototype.calPitagoras = function () {
        this.showres = true;
        if (this.varOne === 'CA') {
            if (this.varTwo === 'CO') {
                this.valueCA = this.valueOne;
                this.valueCO = this.valueTwo;
                this.valueH = Math.sqrt((this.valueCA * this.valueCA) + (this.valueCO * this.valueCO)).toFixed(5);
                this.valueTH = this.degress(Math.atan(this.valueCO / this.valueCA)).toFixed(5);
            }
            else if (this.varTwo === 'H') {
                this.valueCA = this.valueOne;
                this.valueH = this.valueTwo;
                this.valueCO = Math.sqrt((this.valueH * this.valueH) - (this.valueCA * this.valueCA)).toFixed(5);
                this.valueTH = this.degress(Math.acos(this.valueCA / this.valueH)).toFixed(5);
            }
            else if (this.varTwo === 'θ') {
                this.valueCA = this.valueOne;
                this.valueTH = this.valueTwo;
                var valueTHR = this.radians(this.valueTwo);
                this.valueCO = (Math.tan(valueTHR) * this.valueCA).toFixed(5);
                this.valueH = Math.sqrt((this.valueCA * this.valueCA) + (this.valueCO * this.valueCO)).toFixed(5);
            }
        }
        else if (this.varOne === 'CO') {
            if (this.varTwo === 'H') {
                this.valueCO = this.valueOne;
                this.valueH = this.valueTwo;
                this.valueCA = Math.sqrt((this.valueH * this.valueH) - (this.valueCO * this.valueCO)).toFixed(5);
                this.valueTH = this.degress(Math.atan(this.valueCO / this.valueCA)).toFixed(5);
            }
            else if (this.varTwo === 'θ') {
                this.valueCO = this.valueOne;
                this.valueTH = this.valueTwo;
                var valueTHR = this.radians(this.valueTwo);
                this.valueCA = (this.valueCO / Math.tan(valueTHR)).toFixed(5);
                this.valueH = Math.sqrt((this.valueCA * this.valueCA) + (this.valueCO * this.valueCO)).toFixed(5);
            }
        }
        else if (this.varOne === 'H') {
            if (this.varTwo === 'θ') {
                this.valueH = this.valueOne;
                this.valueTH = this.valueTwo;
                var valueTHR = this.radians(this.valueTwo);
                this.valueCO = (this.valueH * Math.sin(valueTHR)).toFixed(5);
                this.valueCA = (this.valueH * Math.cos(valueTHR)).toFixed(5);
            }
        }
    };
    PitagorasPage.prototype.degress = function (radians) {
        return radians * 180 / Math.PI;
    };
    PitagorasPage.prototype.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };
    ;
    PitagorasPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-pitagoras',
            templateUrl: 'pitagoras.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AlertController])
    ], PitagorasPage);
    return PitagorasPage;
}());
export { PitagorasPage };
//# sourceMappingURL=pitagoras.js.map