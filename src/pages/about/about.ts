import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  pita:boolean = false;
  sincos:boolean = false;
  ecuation:boolean = false;
  matri:boolean = false;

  constructor(public navCtrl: NavController) {
  }

  changePita(){
  this.sincos = false;
  this.ecuation = false;
  this.matri = false;
	if (this.pita === false) {
		this.pita = true;
	} else {
		this.pita = false;
	}
  }
  changeSinCos(){
  	this.pita = false;
  	this.ecuation = false;
  	this.matri = false;
  		if (this.sincos === false) {
  			this.sincos = true;
  		} else {
  			this.sincos = false;
  		}
  	}	

  changeEcuation(){
  	this.pita = false;
  	this.sincos = false;
  	this.matri = false;
  		if (this.ecuation === false) {
  			this.ecuation = true;
  		} else {
  			this.ecuation = false;
  		}
  	}

  	changeMatri(){
  	this.pita = false;
  	this.sincos = false;
  	this.ecuation = false;
  		if (this.matri === false) {
  			this.matri = true;
  		} else {
  			this.matri = false;
  		}
  	}

}
