import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as math from 'mathjs';
import * as Plotly from 'plotly.js';

/**
 * Generated class for the PlotfunPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-plotfun',
  templateUrl: 'plotfun.html',
})

export class PlotfunPage {
  eq:string = '4 * sin(x/10) + 5 * cos(x/20)';
  structure: any = { lower: -100, upper: 100 };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  draw() {
  	var expression = this.eq;
    var expr = math.compile(expression);
    console.log(expr);
    var xvalues = math.range(this.structure.lower, this.structure.upper, 0.1);
	var yvalues = xvalues.map(function(x) {
   		return expr.eval({x: x});
	});
	console.log(yvalues);

	var data:any = [{
	    x: xvalues.toArray(),
	    y: yvalues.toArray(),
	    type: 'scatter'
	}];

  var layout = {
  margin: {
    l: 20,
    r: 20,
    b: 20,
    t: 20,
    pad: 2
  }
};

	Plotly.newPlot('plot', data, layout);

  }

}
