import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlotfunPage } from './plotfun';

@NgModule({
  declarations: [
    PlotfunPage,
  ],
  imports: [
    IonicPageModule.forChild(PlotfunPage),
  ],
})
export class PlotfunPageModule {}
