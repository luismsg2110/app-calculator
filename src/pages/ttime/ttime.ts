import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TtimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ttime',
  templateUrl: 'ttime.html',
})
export class TtimePage {
  unitone:string = 'ns';
  valueu:number = 0;
  show:boolean = false;

  valuens:number = 0;
  valuems:number = 0;
  valuemis:number = 0;
  valuesg:number = 0;
  valuemin:number = 0;
  valuehr:number = 0;
  valuedi:number = 0;
  valuesm:number = 0;
  valueao:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calUnits(){
  	this.show = true;
  	if ( this.unitone === 'ns'){
  		this.valuens = this.valueu;
  		this.valuems = this.valuens/1000;
  		this.valuemis = this.valuems/1000;
  		this.valuesg = this.valuemis/1000;
  		this.valuemin = this.valuesg/60;
  		this.valuehr = this.valuemin/60;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'ms') {
  		this.valuems = this.valueu;
  		this.valuens = this.valuems*1000;
  		this.valuemis = this.valuems/1000;
  		this.valuesg = this.valuemis/1000;
  		this.valuemin = this.valuesg/60;
  		this.valuehr = this.valuemin/60;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'mis') {
  		this.valuemis = this.valueu;
  		this.valuens = this.valuemis*1000000;
  		this.valuems = this.valuemis*1000;
  		this.valuesg = this.valuemis/1000;
  		this.valuemin = this.valuesg/60;
  		this.valuehr = this.valuemin/60;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;	
  	} else if ( this.unitone === 'sg') {
  		this.valuesg = this.valueu;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valuemin = this.valuesg/60;
  		this.valuehr = this.valuemin/60;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'min'){
  		this.valuemin = this.valueu;
  		this.valuesg = this.valuemin*60;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valuehr = this.valuemin/60;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'hr') {
  		this.valuehr = this.valueu;
  		this.valuemin = this.valuehr*60;
  		this.valuesg = this.valuemin*60;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valuedi = this.valuehr/24;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'di') {
  		this.valuedi = this.valueu;
  		this.valuehr = this.valuedi*24;
  		this.valuemin = this.valuehr*60;
  		this.valuesg = this.valuemin*60;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valuesm = this.valuedi/7;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'sm') {
  		this.valuesm = this.valueu;
  		this.valuedi = this.valuesm*7;
  		this.valuehr = this.valuedi*24;
  		this.valuemin = this.valuehr*60;
  		this.valuesg = this.valuemin*60;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valueao = this.valuedi/365;
  	} else if ( this.unitone === 'ao') {
  		this.valueao = this.valueu;
  		this.valuedi = this.valueao*365;
  		this.valuehr = this.valuedi*24;
  		this.valuemin = this.valuehr*60;
  		this.valuesg = this.valuemin*60;
  		this.valuens = this.valuesg*1000000000;
  		this.valuems = this.valuesg*1000000;
  		this.valuemis = this.valuesg*1000;
  		this.valuesm = this.valuedi/7;
  	}
  }
}
