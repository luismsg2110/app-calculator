import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TtimePage } from './ttime';

@NgModule({
  declarations: [
    TtimePage,
  ],
  imports: [
    IonicPageModule.forChild(TtimePage),
  ],
})
export class TtimePageModule {}
