var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TtimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TtimePage = /** @class */ (function () {
    function TtimePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.unitone = 'ns';
        this.valueu = 0;
        this.show = false;
        this.valuens = 0;
        this.valuems = 0;
        this.valuemis = 0;
        this.valuesg = 0;
        this.valuemin = 0;
        this.valuehr = 0;
        this.valuedi = 0;
        this.valuesm = 0;
        this.valueao = 0;
    }
    TtimePage.prototype.calUnits = function () {
        this.show = true;
        if (this.unitone === 'ns') {
            this.valuens = this.valueu;
            this.valuems = this.valuens / 1000;
            this.valuemis = this.valuems / 1000;
            this.valuesg = this.valuemis / 1000;
            this.valuemin = this.valuesg / 60;
            this.valuehr = this.valuemin / 60;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'ms') {
            this.valuems = this.valueu;
            this.valuens = this.valuems * 1000;
            this.valuemis = this.valuems / 1000;
            this.valuesg = this.valuemis / 1000;
            this.valuemin = this.valuesg / 60;
            this.valuehr = this.valuemin / 60;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'mis') {
            this.valuemis = this.valueu;
            this.valuens = this.valuemis * 1000000;
            this.valuems = this.valuemis * 1000;
            this.valuesg = this.valuemis / 1000;
            this.valuemin = this.valuesg / 60;
            this.valuehr = this.valuemin / 60;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'sg') {
            this.valuesg = this.valueu;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valuemin = this.valuesg / 60;
            this.valuehr = this.valuemin / 60;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'min') {
            this.valuemin = this.valueu;
            this.valuesg = this.valuemin * 60;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valuehr = this.valuemin / 60;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'hr') {
            this.valuehr = this.valueu;
            this.valuemin = this.valuehr * 60;
            this.valuesg = this.valuemin * 60;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valuedi = this.valuehr / 24;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'di') {
            this.valuedi = this.valueu;
            this.valuehr = this.valuedi * 24;
            this.valuemin = this.valuehr * 60;
            this.valuesg = this.valuemin * 60;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valuesm = this.valuedi / 7;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'sm') {
            this.valuesm = this.valueu;
            this.valuedi = this.valuesm * 7;
            this.valuehr = this.valuedi * 24;
            this.valuemin = this.valuehr * 60;
            this.valuesg = this.valuemin * 60;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valueao = this.valuedi / 365;
        }
        else if (this.unitone === 'ao') {
            this.valueao = this.valueu;
            this.valuedi = this.valueao * 365;
            this.valuehr = this.valuedi * 24;
            this.valuemin = this.valuehr * 60;
            this.valuesg = this.valuemin * 60;
            this.valuens = this.valuesg * 1000000000;
            this.valuems = this.valuesg * 1000000;
            this.valuemis = this.valuesg * 1000;
            this.valuesm = this.valuedi / 7;
        }
    };
    TtimePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-ttime',
            templateUrl: 'ttime.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TtimePage);
    return TtimePage;
}());
export { TtimePage };
//# sourceMappingURL=ttime.js.map