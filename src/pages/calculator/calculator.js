var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the CalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CalculatorPage = /** @class */ (function () {
    function CalculatorPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.memory = [];
        this.operator = 0;
        this.operand = 0;
        this.result = 0;
    }
    CalculatorPage.prototype.numbers = function (x) {
        this.memory.push(x);
        this.result = this.memory.join('') * 1;
    };
    CalculatorPage.prototype.operation = function (x) {
        this.operand = this.result;
        this.memory = [];
        this.operator = x;
    };
    CalculatorPage.prototype.clear = function () {
        this.result = 0;
        this.operand = 0;
        this.memory = [];
    };
    CalculatorPage.prototype.equals = function () {
        if (this.operator === 1) {
            this.result += this.operand;
        }
        else if (this.operator === 2) {
            this.result = this.operand - this.result;
        }
        else if (this.operator === 3) {
            this.result *= this.operand;
        }
        else if (this.operator === 4) {
            this.result = this.operand / this.result;
        }
        else if (this.operator === 5) {
            this.result = Math.pow(this.operand, this.result);
        }
        else if (this.operator === 6) {
            this.result = this.operand * this.result / 100;
        }
    };
    CalculatorPage.prototype.squared = function () {
        this.result *= this.result;
    };
    CalculatorPage.prototype.squareRoot = function () {
        this.result = Math.sqrt(this.result);
    };
    CalculatorPage.prototype.log = function () {
        this.result = Math.log10(this.result);
    };
    CalculatorPage.prototype.sine = function () {
        this.result = Math.sin(this.result);
    };
    CalculatorPage.prototype.cosine = function () {
        this.result = Math.cos(this.result);
    };
    CalculatorPage.prototype.tangent = function () {
        this.result = Math.tan(this.result);
    };
    CalculatorPage.prototype.arcsine = function () {
        this.result = Math.asin(this.result);
    };
    CalculatorPage.prototype.arccosine = function () {
        this.result = Math.acos(this.result);
    };
    CalculatorPage.prototype.arctangent = function () {
        this.result = Math.atan(this.result);
    };
    CalculatorPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-calculator',
            templateUrl: 'calculator.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], CalculatorPage);
    return CalculatorPage;
}());
export { CalculatorPage };
//# sourceMappingURL=calculator.js.map