import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as mathe from 'mathjs';

/**
 * Generated class for the CalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})

export class CalculatorPage {
	memory:any = [];
	operator:any = 0;
	operand:any = 0;
	result:any = 0;

 	constructor(public navCtrl: NavController, public navParams: NavParams) {
 	}

	numbers(x) {
		this.memory.push(x);
		this.result = this.memory.join('') * 1;
	}

	operation(x) {
    	this.operand = this.result;
    	this.memory = [];
    	this.operator = x;
 	}

 	clear() {
	    this.result = 0;
	    this.operand = 0;
	    this.memory = [];
	}

	equals() {
	    if (this.operator === 1) {
	      this.result = mathe.round(this.result+this.operand, 5);
	    } else if (this.operator === 2) {
	      this.result = mathe.round(this.operand - this.result, 5);
	    } else if (this.operator === 3) {
	      this.result = mathe.round(this.result*this.operand, 5);
	    } else if (this.operator === 4) {
	      this.result = mathe.round(this.operand / this.result, 5);
	    } else if (this.operator === 5) {
	      this.result = mathe.round(Math.pow(this.operand , this.result), 5);
	    } else if (this.operator === 6) {
	      this.result = mathe.round(this.operand*this.result/100, 5);
	    }
 	}

 	squared() {
    	this.result = mathe.round(this.result*this.result, 5);
	}

	squareRoot() {
    	this.result = mathe.round(Math.sqrt(this.result), 5);
  	}

  	log() {
    	this.result = mathe.round(Math.log10(this.result), 5);
  	}

  	sine() {
    	this.result = mathe.round(Math.sin(this.radians(this.result)), 5);
	}
  	cosine() {
    	this.result = mathe.round(Math.cos(this.radians(this.result)), 5);
  	}
  	tangent() {
    	this.result = mathe.round(Math.tan(this.radians(this.result)), 5);
  	}
  	arcsine() {
    	this.result = mathe.round(this.degress(Math.asin(this.result)), 5);
  	}
  	arccosine() {
    	this.result = mathe.round(this.degress(Math.acos(this.result)), 5);
  	}
  	arctangent() {
    	this.result = mathe.round(this.degress(Math.atan(this.result)), 5);
  	}

  	degress(radians){
	  	return radians * 180 / Math.PI;
	}

	radians(degrees) {
		return degrees * Math.PI / 180;
	};

}
