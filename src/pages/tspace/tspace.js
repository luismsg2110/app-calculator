var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TspacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TspacePage = /** @class */ (function () {
    function TspacePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.unitone = 'mm';
        this.valueu = 0;
        this.show = false;
        this.valuemm = 0;
        this.valuecm = 0;
        this.valuemt = 0;
        this.valuekm = 0;
        this.valuepg = 0;
        this.valueft = 0;
        this.valueyd = 0;
        this.valueml = 0;
    }
    TspacePage.prototype.calUnits = function () {
        this.show = true;
        if (this.unitone === 'mm') {
            this.valuemm = this.valueu;
            this.valuecm = this.valueu / 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valuemt = this.valuecm / 100;
            this.valuekm = this.valuemt / 1000;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'cm') {
            this.valuemm = this.valueu * 10;
            this.valuecm = this.valueu;
            this.valuepg = this.valuecm * 0.393701;
            this.valuemt = this.valuecm / 100;
            this.valuekm = this.valuemt / 1000;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'mt') {
            this.valuemt = this.valueu;
            this.valuecm = this.valuemt * 100;
            this.valuemm = this.valuecm * 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valuekm = this.valuemt / 1000;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'km') {
            this.valuekm = this.valueu;
            this.valuemt = this.valuekm * 1000;
            this.valuecm = this.valuemt * 100;
            this.valuemm = this.valuecm * 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'pg') {
            this.valuepg = this.valueu;
            this.valuecm = this.valuepg / 0.393701;
            this.valuemm = this.valuecm * 10;
            this.valuemt = this.valuecm / 100;
            this.valuekm = this.valuemt / 1000;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'ft') {
            this.valueft = this.valueu;
            this.valuemt = this.valueft / 3.28084;
            this.valuecm = this.valuemt * 100;
            this.valuemm = this.valuecm * 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valuekm = this.valuemt / 1000;
            this.valueyd = this.valuemt * 1.09361;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'yd') {
            this.valueyd = this.valueu;
            this.valuemt = this.valueyd / 1.09361;
            this.valuecm = this.valuemt * 100;
            this.valuemm = this.valuecm * 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valuekm = this.valuemt / 1000;
            this.valueft = this.valuemt * 3.28084;
            this.valueml = this.valuekm * 0.539957;
        }
        else if (this.unitone === 'ml') {
            this.valueml = this.valueu;
            this.valuekm = this.valueml / 0.539957;
            this.valuemt = this.valuekm * 1000;
            this.valuecm = this.valuemt * 100;
            this.valuemm = this.valuecm * 10;
            this.valuepg = this.valuecm * 0.393701;
            this.valueyd = this.valuemt * 1.09361;
            this.valueft = this.valuemt * 3.28084;
        }
    };
    TspacePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tspace',
            templateUrl: 'tspace.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TspacePage);
    return TspacePage;
}());
export { TspacePage };
//# sourceMappingURL=tspace.js.map