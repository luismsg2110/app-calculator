import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TspacePage } from './tspace';

@NgModule({
  declarations: [
    TspacePage,
  ],
  imports: [
    IonicPageModule.forChild(TspacePage),
  ],
})
export class TspacePageModule {}
