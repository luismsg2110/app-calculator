import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TspacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tspace',
  templateUrl: 'tspace.html',
})
export class TspacePage {
  unitone:string = 'mm';
  valueu:number = 0;
  show:boolean = false;

  valuemm:number = 0;
  valuecm:number = 0;
  valuemt:number = 0;
  valuekm:number = 0;
  valuepg:number = 0;
  valueft:number = 0;
  valueyd:number = 0;
  valueml:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calUnits(){
  	this.show = true;
  	if ( this.unitone === 'mm'){
  		this.valuemm = this.valueu;
  		this.valuecm = this.valueu/10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valuemt = this.valuecm/100;
  		this.valuekm = this.valuemt/1000;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'cm') {
  		this.valuemm = this.valueu*10;
  		this.valuecm = this.valueu;
  		this.valuepg = this.valuecm*0.393701;
  		this.valuemt = this.valuecm/100;
  		this.valuekm = this.valuemt/1000;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'mt') {
  		this.valuemt = this.valueu;
  		this.valuecm = this.valuemt*100;
  		this.valuemm = this.valuecm*10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valuekm = this.valuemt/1000;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'km') {
  		this.valuekm = this.valueu;
  		this.valuemt = this.valuekm*1000;
  		this.valuecm = this.valuemt*100;
  		this.valuemm = this.valuecm*10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'pg'){
  		this.valuepg = this.valueu;
  		this.valuecm = this.valuepg/0.393701;
  		this.valuemm = this.valuecm*10;
  		this.valuemt = this.valuecm/100;
  		this.valuekm = this.valuemt/1000;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'ft') {
  		this.valueft = this.valueu;
  		this.valuemt = this.valueft/3.28084;
  		this.valuecm = this.valuemt*100;
  		this.valuemm = this.valuecm*10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valuekm = this.valuemt/1000;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'yd') {
  		this.valueyd = this.valueu;
  		this.valuemt = this.valueyd/1.09361;
  		this.valuecm = this.valuemt*100;
  		this.valuemm = this.valuecm*10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valuekm = this.valuemt/1000;
  		this.valueft = this.valuemt*3.28084;
  		this.valueml = this.valuekm*0.539957;
  	} else if ( this.unitone === 'ml') {
  		this.valueml = this.valueu;
  		this.valuekm =  this.valueml/0.539957;
  		this.valuemt = this.valuekm*1000;
  		this.valuecm = this.valuemt*100;
  		this.valuemm = this.valuecm*10;
  		this.valuepg = this.valuecm*0.393701;
  		this.valueyd = this.valuemt*1.09361;
  		this.valueft = this.valuemt*3.28084;
  	}
  }



}
