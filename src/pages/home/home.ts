import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CalculatorPage } from '../calculator/calculator';
import { ComplexPage } from '../complex/complex';
import { MatrixPage} from '../matrix/matrix';
import { PitagorasPage } from '../pitagoras/pitagoras';
import { EquationtwoPage } from '../equationtwo/equationtwo';
import { EquationthreePage } from '../equationthree/equationthree';
import { TspacePage } from '../tspace/tspace';
import { TtimePage } from '../ttime/ttime';
import { TmasaPage } from '../tmasa/tmasa';
import { TtemperaturePage } from '../ttemperature/ttemperature';
import { SincosPage } from '../sincos/sincos';
import { PlotfunPage } from '../plotfun/plotfun';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  calculatorPage = CalculatorPage;
  complexPage = ComplexPage;
  matrixPage = MatrixPage;
  pitagorasPage = PitagorasPage;
  equationtwoPage = EquationtwoPage;
  equationthreePage = EquationthreePage;
  tspacePage = TspacePage;
  ttimePage = TtimePage;
  tmasaPage = TmasaPage;
  ttemperaturePage = TtemperaturePage;
  sincosPage = SincosPage;
  plotfunPage = PlotfunPage;

  constructor(public navCtrl: NavController) {

  }

}
