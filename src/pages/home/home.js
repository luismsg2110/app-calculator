var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CalculatorPage } from '../calculator/calculator';
import { ComplexPage } from '../complex/complex';
import { MatrixPage } from '../matrix/matrix';
import { PitagorasPage } from '../pitagoras/pitagoras';
import { EquationtwoPage } from '../equationtwo/equationtwo';
import { EquationthreePage } from '../equationthree/equationthree';
import { TspacePage } from '../tspace/tspace';
import { TtimePage } from '../ttime/ttime';
import { TmasaPage } from '../tmasa/tmasa';
import { TtemperaturePage } from '../ttemperature/ttemperature';
import { SincosPage } from '../sincos/sincos';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.calculatorPage = CalculatorPage;
        this.complexPage = ComplexPage;
        this.matrixPage = MatrixPage;
        this.pitagorasPage = PitagorasPage;
        this.equationtwoPage = EquationtwoPage;
        this.equationthreePage = EquationthreePage;
        this.tspacePage = TspacePage;
        this.ttimePage = TtimePage;
        this.tmasaPage = TmasaPage;
        this.ttemperaturePage = TtemperaturePage;
        this.sincosPage = SincosPage;
    }
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map