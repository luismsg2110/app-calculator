var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TmasaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TmasaPage = /** @class */ (function () {
    function TmasaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.unitone = 'mg';
        this.valueu = 0;
        this.show = false;
        this.valuemg = 0;
        this.valuegr = 0;
        this.valuekg = 0;
        this.valuetn = 0;
        this.valuelb = 0;
        this.valueoz = 0;
    }
    TmasaPage.prototype.calUnits = function () {
        this.show = true;
        if (this.unitone === 'mg') {
            this.valuemg = this.valueu;
            this.valuegr = this.valuemg / 1000;
            this.valuekg = this.valuegr / 1000;
            this.valuetn = this.valuekg / 1000;
            this.valuelb = this.valuekg * 2.20462;
            this.valueoz = this.valuekg * 35.274;
        }
        else if (this.unitone === 'gr') {
            this.valuegr = this.valueu;
            this.valuemg = this.valuegr * 1000;
            this.valuekg = this.valuegr / 1000;
            this.valuetn = this.valuekg / 1000;
            this.valuelb = this.valuekg * 2.20462;
            this.valueoz = this.valuekg * 35.274;
        }
        else if (this.unitone === 'kg') {
            this.valuekg = this.valueu;
            this.valuegr = this.valuekg * 1000;
            this.valuemg = this.valuegr * 1000;
            this.valuetn = this.valuekg / 1000;
            this.valuelb = this.valuekg * 2.20462;
            this.valueoz = this.valuekg * 35.274;
        }
        else if (this.unitone === 'tn') {
            this.valuetn = this.valueu;
            this.valuekg = this.valuetn * 1000;
            this.valuegr = this.valuekg * 1000;
            this.valuemg = this.valuegr * 1000;
            this.valuelb = this.valuekg * 2.20462;
            this.valueoz = this.valuekg * 35.274;
        }
        else if (this.unitone === 'lb') {
            this.valuelb = this.valueu;
            this.valuekg = this.valuelb / 2.20462;
            this.valuegr = this.valuekg * 1000;
            this.valuemg = this.valuegr * 1000;
            this.valuetn = this.valuekg / 1000;
            this.valueoz = this.valuelb * 16;
        }
        else if (this.unitone === 'oz') {
            this.valueoz = this.valueu;
            this.valuekg = this.valueoz / 35.274;
            this.valuegr = this.valuekg * 1000;
            this.valuemg = this.valuegr * 1000;
            this.valuetn = this.valuekg / 1000;
            this.valuelb = this.valuekg / 16;
        }
    };
    TmasaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tmasa',
            templateUrl: 'tmasa.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TmasaPage);
    return TmasaPage;
}());
export { TmasaPage };
//# sourceMappingURL=tmasa.js.map