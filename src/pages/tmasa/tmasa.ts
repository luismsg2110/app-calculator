import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TmasaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tmasa',
  templateUrl: 'tmasa.html',
})
export class TmasaPage {
  unitone:string = 'mg';
  valueu:number = 0;
  show:boolean = false;

  valuemg:number = 0;
  valuegr:number = 0;
  valuekg:number = 0;
  valuetn:number = 0;
  valuelb:number = 0;
  valueoz:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calUnits(){
  	this.show = true;
  	if ( this.unitone === 'mg'){
  		this.valuemg = this.valueu;
  		this.valuegr = this.valuemg/1000;
  		this.valuekg = this.valuegr/1000;
  		this.valuetn = this.valuekg/1000;
  		this.valuelb = this.valuekg*2.20462;
  		this.valueoz = this.valuekg*35.274;
  	} else if ( this.unitone === 'gr') {
  		this.valuegr = this.valueu;
  		this.valuemg = this.valuegr*1000;
  		this.valuekg = this.valuegr/1000;
  		this.valuetn = this.valuekg/1000;
  		this.valuelb = this.valuekg*2.20462;
  		this.valueoz = this.valuekg*35.274;
  	} else if ( this.unitone === 'kg') {
  		this.valuekg = this.valueu;
  		this.valuegr = this.valuekg*1000;
  		this.valuemg = this.valuegr*1000;
  		this.valuetn = this.valuekg/1000;
  		this.valuelb = this.valuekg*2.20462;
  		this.valueoz = this.valuekg*35.274;
  	} else if ( this.unitone === 'tn') {
  		this.valuetn = this.valueu;
  		this.valuekg = this.valuetn*1000;
  		this.valuegr = this.valuekg*1000;
  		this.valuemg = this.valuegr*1000;
  		this.valuelb = this.valuekg*2.20462;
  		this.valueoz = this.valuekg*35.274;
  	} else if ( this.unitone === 'lb'){
  		this.valuelb = this.valueu;
  		this.valuekg = this.valuelb/2.20462;
  		this.valuegr = this.valuekg*1000;
  		this.valuemg = this.valuegr*1000;
  		this.valuetn = this.valuekg/1000;
  		this.valueoz = this.valuelb*16;
  	} else if ( this.unitone === 'oz') {
  		this.valueoz = this.valueu;
  		this.valuekg = this.valueoz/35.274;
  		this.valuegr = this.valuekg*1000;
  		this.valuemg = this.valuegr*1000;
  		this.valuetn = this.valuekg/1000;
  		this.valuelb = this.valuekg/16;
  	}
  }

}
