import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TmasaPage } from './tmasa';

@NgModule({
  declarations: [
    TmasaPage,
  ],
  imports: [
    IonicPageModule.forChild(TmasaPage),
  ],
})
export class TmasaPageModule {}
