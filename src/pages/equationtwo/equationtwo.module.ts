import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EquationtwoPage } from './equationtwo';

@NgModule({
  declarations: [
    EquationtwoPage,
  ],
  imports: [
    IonicPageModule.forChild(EquationtwoPage),
  ],
})
export class EquationtwoPageModule {}
