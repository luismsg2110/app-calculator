import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EquationtwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-equationtwo',
  templateUrl: 'equationtwo.html',
})
export class EquationtwoPage {
  valuea:number = 0;
  valueb:number = 0;
  valuec:number = 0;
  x1:number = 0;
  x2:number = 0;
  xi1:number = 0;
  xi2:number = 0;
  xj1:number = 0;
  xj2:number = 0;
  showreal:boolean = false;
  showcomplex:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calEquationTwo(){
  	var discri = (this.valueb*this.valueb) - (4*this.valuea*this.valuec);
  	if (discri < 0) {
  		this.showreal = false;
  		this.showcomplex = true;
  		this.xi1 = (-1*this.valueb)/(2*this.valuea);
  		this.xj1 = Math.sqrt(Math.abs(discri))/(2*this.valuea);
  	} else {
  		this.showcomplex = false;
  		this.showreal = true;
  		this.x1 = ((-1*this.valueb) + Math.sqrt(discri))/(2*this.valuea);
  		this.x2 = ((-1*this.valueb) - Math.sqrt(discri))/(2*this.valuea);
  	}
  }

}
