var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the EquationtwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EquationtwoPage = /** @class */ (function () {
    function EquationtwoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.valuea = 0;
        this.valueb = 0;
        this.valuec = 0;
        this.x1 = 0;
        this.x2 = 0;
        this.xi1 = 0;
        this.xi2 = 0;
        this.xj1 = 0;
        this.xj2 = 0;
        this.showreal = false;
        this.showcomplex = false;
    }
    EquationtwoPage.prototype.calEquationTwo = function () {
        var discri = (this.valueb * this.valueb) - (4 * this.valuea * this.valuec);
        if (discri < 0) {
            this.showreal = false;
            this.showcomplex = true;
            this.xi1 = (-1 * this.valueb) / (2 * this.valuea);
            this.xj1 = Math.sqrt(Math.abs(discri)) / (2 * this.valuea);
        }
        else {
            this.showcomplex = false;
            this.showreal = true;
            this.x1 = ((-1 * this.valueb) + Math.sqrt(discri)) / (2 * this.valuea);
            this.x2 = ((-1 * this.valueb) - Math.sqrt(discri)) / (2 * this.valuea);
        }
    };
    EquationtwoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-equationtwo',
            templateUrl: 'equationtwo.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], EquationtwoPage);
    return EquationtwoPage;
}());
export { EquationtwoPage };
//# sourceMappingURL=equationtwo.js.map