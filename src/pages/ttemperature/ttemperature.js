var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TtemperaturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TtemperaturePage = /** @class */ (function () {
    function TtemperaturePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.unitone = 'gc';
        this.valueu = 0;
        this.show = false;
        this.valuegc = 0;
        this.valuegf = 0;
        this.valuegk = 0;
    }
    TtemperaturePage.prototype.calUnits = function () {
        this.show = true;
        if (this.unitone === 'gc') {
            this.valuegc = this.valueu;
            this.valuegf = (this.valuegc * 1.8) + 32;
            this.valuegk = Number(this.valuegc) + 273.15;
        }
        else if (this.unitone === 'gf') {
            this.valuegf = this.valueu;
            this.valuegc = (this.valuegf - 32) / 1.8;
            this.valuegk = this.valuegc + 273.15;
        }
        else if (this.unitone === 'gk') {
            this.valuegk = this.valueu;
            this.valuegc = this.valuegk - 273.15;
            this.valuegf = (this.valuegc * 1.8) + 32;
        }
    };
    TtemperaturePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-ttemperature',
            templateUrl: 'ttemperature.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TtemperaturePage);
    return TtemperaturePage;
}());
export { TtemperaturePage };
//# sourceMappingURL=ttemperature.js.map