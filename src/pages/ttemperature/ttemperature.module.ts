import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TtemperaturePage } from './ttemperature';

@NgModule({
  declarations: [
    TtemperaturePage,
  ],
  imports: [
    IonicPageModule.forChild(TtemperaturePage),
  ],
})
export class TtemperaturePageModule {}
