import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TtemperaturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ttemperature',
  templateUrl: 'ttemperature.html',
})
export class TtemperaturePage {

  unitone:string = 'gc';
  valueu:number = 0;
  show:boolean = false;

  valuegc:number = 0;
  valuegf:number = 0;
  valuegk:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  calUnits(){
  	this.show = true;
  	if ( this.unitone === 'gc'){
  		this.valuegc = this.valueu;
  		this.valuegf = (this.valuegc*1.8) + 32;
  		this.valuegk = Number(this.valuegc) + 273.15;
  	} else if ( this.unitone === 'gf') {
		this.valuegf = this.valueu;
		this.valuegc = (this.valuegf-32)/1.8;
		this.valuegk = this.valuegc + 273.15;
  	} else if ( this.unitone === 'gk') {
  		this.valuegk = this.valueu;
  		this.valuegc = this.valuegk - 273.15;
  		this.valuegf = (this.valuegc*1.8) + 32;
  	} 
  }

}
