import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the ComplexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complex',
  templateUrl: 'complex.html',
})
export class ComplexPage {
  operator:any = '?';
  color:any = 'red';
  show:boolean = false;
  valores : FormGroup;
  real:number = 0;
  img:number = 0;
  sign:any = '+';
  a1:number = 0;
  a2:number = 0;
  b1:number = 0;
  b2:number = 0;
  imgr:number = 0;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
  	this.valores = this.formBuilder.group({
      val1: ['', Validators.required],
      val2: ['', Validators.required],
      val3: ['', Validators.required],
      val4: ['', Validators.required],
    });
  }

  onChange(operation){
  	this.color = 'green';
  	this.show = true;
  	if (operation === 'sum') {
  		this.operator = '+';
  	} else if (operation === 'rest') {
  		this.operator = '-';
  	} else if (operation === 'multi') {
  		this.operator = 'x';
  	} else if (operation === 'div') {
  		this.operator = '/';
  	}
  }

  calForm() {
  	this.a1 = Number(this.a1);
  	this.a2 = Number(this.a2);
  	this.b1 = Number(this.b1);
  	this.b2 = Number(this.b2);
  	if (this.operator === '+'){
  		this.real = this.a1 + this.a2;
  		this.imgr = this.b1 + this.b2;
  		this.img = Math.abs(this.imgr);
  		this.sign = this.getSign(this.imgr);
  	} else if (this.operator === '-'){
  		this.real = this.a1 - this.a2;
  		this.imgr = this.b1 - this.b2;
  		this.img = Math.abs(this.imgr);
  		this.sign = this.getSign(this.imgr);
    } else if (this.operator === 'x'){
    	this.real = (this.a1*this.a2) - (this.b1*this.b2);
    	this.imgr = (this.a1*this.b2) + (this.a2*this.b1);
    	this.img = Math.abs(this.imgr);
    	this.sign = this.getSign(this.imgr);
  	} else if (this.operator == '/'){
  		this.real = ((this.a1*this.a2) + (this.b1*this.b2))/((this.a2*this.a2)+(this.b2*this.b2));
  		this.imgr = ((this.a2*this.b1) - (this.a1*this.b2))/((this.a2*this.a2)+(this.b2*this.b2));
  		this.img = Math.abs(this.imgr);
    	this.sign = this.getSign(this.imgr);
  	}
  }

  private getSign(val){
  	if (val < 0) {
  		return '-';
  	} else {
  		return '+';
  	}
  }

}
