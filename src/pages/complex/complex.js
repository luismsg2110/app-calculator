var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
/**
 * Generated class for the ComplexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ComplexPage = /** @class */ (function () {
    function ComplexPage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.operator = '?';
        this.color = 'red';
        this.show = false;
        this.real = 0;
        this.img = 0;
        this.sign = '+';
        this.a1 = 0;
        this.a2 = 0;
        this.b1 = 0;
        this.b2 = 0;
        this.imgr = 0;
        this.valores = this.formBuilder.group({
            val1: ['', Validators.required],
            val2: ['', Validators.required],
            val3: ['', Validators.required],
            val4: ['', Validators.required],
        });
    }
    ComplexPage.prototype.onChange = function (operation) {
        this.color = 'green';
        this.show = true;
        if (operation === 'sum') {
            this.operator = '+';
        }
        else if (operation === 'rest') {
            this.operator = '-';
        }
        else if (operation === 'multi') {
            this.operator = 'x';
        }
        else if (operation === 'div') {
            this.operator = '/';
        }
    };
    ComplexPage.prototype.calForm = function () {
        this.a1 = Number(this.a1);
        this.a2 = Number(this.a2);
        this.b1 = Number(this.b1);
        this.b2 = Number(this.b2);
        if (this.operator === '+') {
            this.real = this.a1 + this.a2;
            this.imgr = this.b1 + this.b2;
            this.img = Math.abs(this.imgr);
            this.sign = this.getSign(this.imgr);
        }
        else if (this.operator === '-') {
            this.real = this.a1 - this.a2;
            this.imgr = this.b1 - this.b2;
            this.img = Math.abs(this.imgr);
            this.sign = this.getSign(this.imgr);
        }
        else if (this.operator === 'x') {
            this.real = (this.a1 * this.a2) - (this.b1 * this.b2);
            this.imgr = (this.a1 * this.b2) + (this.a2 * this.b1);
            this.img = Math.abs(this.imgr);
            this.sign = this.getSign(this.imgr);
        }
        else if (this.operator == '/') {
            this.real = ((this.a1 * this.a2) + (this.b1 * this.b2)) / ((this.a2 * this.a2) + (this.b2 * this.b2));
            this.imgr = ((this.a2 * this.b1) - (this.a1 * this.b2)) / ((this.a2 * this.a2) + (this.b2 * this.b2));
            this.img = Math.abs(this.imgr);
            this.sign = this.getSign(this.imgr);
        }
    };
    ComplexPage.prototype.getSign = function (val) {
        if (val < 0) {
            return '-';
        }
        else {
            return '+';
        }
    };
    ComplexPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-complex',
            templateUrl: 'complex.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, FormBuilder])
    ], ComplexPage);
    return ComplexPage;
}());
export { ComplexPage };
//# sourceMappingURL=complex.js.map