import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplexPage } from './complex';

@NgModule({
  declarations: [
    ComplexPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplexPage),
  ],
})
export class ComplexPageModule {}
