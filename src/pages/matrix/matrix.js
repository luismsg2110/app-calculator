var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the MatrixPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MatrixPage = /** @class */ (function () {
    function MatrixPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.operator = '?';
        this.color = 'red';
        this.show = false;
        this.showOne = false;
        this.showTwo = false;
        this.resShow = false;
        this.rows = 2;
        this.colums = 2;
        this.cellsA = [[]];
        this.cellsB = [[]];
        this.result = [[]];
        //sum:any = [];
        this.length = 2;
        this.width = 2;
        this.lengthA = 2;
        this.widthA = 2;
        this.lengthB = 2;
        this.widthB = 2;
        this.row = 0;
        this.cell = 0;
    }
    MatrixPage.prototype.onChange = function (operation) {
        this.color = 'green';
        this.show = true;
        this.resShow = false;
        this.lengthB = this.widthA;
        if (operation === 'sum') {
            this.operator = '+';
            this.showOne = true;
            this.showTwo = false;
        }
        else if (operation === 'rest') {
            this.operator = '-';
            this.showOne = true;
            this.showTwo = false;
        }
        else if (operation === 'multi') {
            this.operator = 'x';
            this.showOne = false;
            this.showTwo = true;
        }
        this.makeMap();
    };
    MatrixPage.prototype.makeMap = function () {
        if (this.operator === '+' || this.operator === '-') {
            var rowsA = this.length;
            var rowsB = this.length;
            var colsA = this.width;
            var colsB = this.width;
        }
        else if (this.operator === 'x') {
            var rowsA = this.lengthA;
            var rowsB = this.lengthB;
            var colsA = this.widthA;
            var colsB = this.widthB;
        }
        this.cellsA = this.matrix(rowsA, colsA, '0');
        this.cellsB = this.matrix(rowsB, colsB, '0');
    };
    MatrixPage.prototype.matrix = function (rows, cols, defaultValue) {
        var arr = [[]];
        for (var i = 0; i < rows; i++) {
            arr[i] = [];
            arr[i] = new Array(cols);
            for (var j = 0; j < cols; j++) {
                arr[i][j] = defaultValue;
            }
        }
        return arr;
    };
    MatrixPage.prototype.calMatrix = function () {
        this.resShow = true;
        if (this.operator === '+') {
            for (var i = 0; i < this.length; i++) {
                this.result[i] = [];
                this.result[i] = new Array(this.width);
                for (var j = 0; j < this.width; j++) {
                    this.result[i][j] = Number(this.cellsA[i][j]) + Number(this.cellsB[i][j]);
                }
            }
        }
        else if (this.operator === '-') {
            for (var i = 0; i < this.length; i++) {
                this.result[i] = [];
                this.result[i] = new Array(this.width);
                for (var j = 0; j < this.width; j++) {
                    this.result[i][j] = Number(this.cellsA[i][j]) - Number(this.cellsB[i][j]);
                }
            }
        }
        else if (this.operator === 'x') {
            for (var i = 0; i < this.lengthA; i++) {
                this.result[i] = [];
                this.result[i] = new Array(this.widthB);
                for (var j = 0; j < this.widthB; j++) {
                    var sum = 0;
                    for (var k = 0; k < this.widthA; k++) {
                        sum += Number(this.cellsA[i][k]) * Number(this.cellsB[k][j]);
                    }
                    this.result[i][j] = sum;
                }
            }
        }
        console.log(this.result);
    };
    MatrixPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-matrix',
            templateUrl: 'matrix.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], MatrixPage);
    return MatrixPage;
}());
export { MatrixPage };
//# sourceMappingURL=matrix.js.map