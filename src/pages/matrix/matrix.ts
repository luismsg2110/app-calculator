import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MatrixPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-matrix',
  templateUrl: 'matrix.html',
})
export class MatrixPage {
  operator:any = '?';
  color:any = 'red';
  show:boolean = false;
  showOne:boolean = false;
  showTwo:boolean = false;
  resShow:boolean = false;
  rows:number = 2;
  colums:number = 2;

  cellsA:any = [[]];
  cellsB:any = [[]];
  result:any = [[]];
  //sum:any = [];
  length:any = 2;
  width:any = 2;
  lengthA:any = 2;
  widthA:any = 2;
  lengthB:any = 2;
  widthB:any = 2;
  row:any = 0;
  cell:any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  onChange(operation){
  	this.color = 'green';
  	this.show = true;
  	this.resShow = false;
  	this.lengthB = this.widthA;
  	if (operation === 'sum') {
  		this.operator = '+';
  		this.showOne = true;
  		this.showTwo = false;
  	} else if (operation === 'rest') {
  		this.operator = '-';
  		this.showOne = true;
  		this.showTwo = false;
  	} else if (operation === 'multi') {
  		this.operator = 'x';
  		this.showOne = false;
  		this.showTwo = true;
  	} 
  	this.makeMap();
    console.log(this.cellsA);
  }

  makeMap() {
  	if (this.operator === '+' || this.operator === '-'){
  		var rowsA = this.length;
  		var rowsB = this.length;
  		var colsA = this.width;
  		var colsB = this.width;
  	} else if (this.operator === 'x'){
  		var rowsA = this.lengthA;
  		var rowsB = this.lengthB;
  		var colsA = this.widthA;
  		var colsB = this.widthB;
  	}
	this.cellsA = this.matrix(rowsA, colsA, 0);
	this.cellsB = this.matrix(rowsB, colsB, 0);
  }

  trackByFn(index: any, item: any) {
   return index;
}

  matrix(rows, cols, defaultValue) {
    var arr = [[]];
    for (var i = 0; i < rows; i++) {
        arr[i] = [];
        arr[i] = new Array(cols);
        for (var j = 0; j < cols; j++) {
            arr[i][j] = defaultValue;
        }
    }
    return arr;
  }

  calMatrix(){
  	this.resShow = true;
  	if (this.operator === '+'){
  		for (var i=0; i < this.length; i++){
	  		this.result[i] = [];
	  		this.result[i] = new Array(this.width);
	  		for (var j=0; j < this.width; j++){
	  			this.result[i][j] = Number(this.cellsA[i][j]) + Number(this.cellsB[i][j]);
	  		}
	  	}
  	} else if (this.operator === '-'){
  		for (var i=0; i < this.length; i++){
	  		this.result[i] = [];
	  		this.result[i] = new Array(this.width);
	  		for (var j=0; j < this.width; j++){
	  			this.result[i][j] = Number(this.cellsA[i][j]) - Number(this.cellsB[i][j]);
	  		}
	  	}
  	} else if (this.operator === 'x'){
  		for ( var i=0; i < this.lengthA; i++){
  			this.result[i] = [];
  			this.result[i] = new Array(this.widthB);
  			for (var j=0; j < this.widthB; j++){
  				 var sum = 0; 
  				for (var k=0; k < this.widthA; k++) {
  					sum += Number(this.cellsA[i][k])*Number(this.cellsB[k][j]);
  				}
  			this.result[i][j] = sum;
  			}

  		}
  	}
    console.log(this.cellsA);
  }

}
